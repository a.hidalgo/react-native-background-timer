# PP500 Mobile

## Desplegar para pruebas (staging)

Utilizarmoes Expo para realizar el proceso.

https://docs.expo.io/versions/latest/guides/release-channels.html

https://docs.expo.io/versions/latest/guides/building-standalone-apps.html#55---uploading-your-ios-ipa-to-testflight

```
exp build:ios --release-channel staging
exp publish --release-channel staging
```

Esto lo podemos hacer igualmente para Android como para iOS y posteriormente a que el proceso termina, podemos subir los compilados a Hockey App.

## Copyright

Desarrollado por [Pixmat Studios](https://www.pixmatstudios.com).
